# Sample Django App

# This repository contains Sample django app with docker file and auto deployment using Jenkins.

## Directory Structure

* sample - django app for sample project
* sampleapp - main django directory contains settings and urls configuration
* templates - contains html template used in project
* .gitignore - git ignore file
* Dockerfile - docker configuration file to create docker containers
* Jenkinsfile - jenkins pipeline script file
* docker-compose.yml docker compose configurations
* manage.py - django manage.py file to manage django commands
* nginx.conf - nginx configuration used on docker
* requirements.txt - django project requirements file
* run.sh - bash script used for running django app inside docker container

## Author: Yogesh Panchal <yspanchal@gmail.com>

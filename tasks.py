from fabric import Connection
from fabric import task

servers = ["54.227.49.164", "100.26.53.63", "100.26.169.241"]
user = "ubuntu"

def get_connection(server, user):
    return Connection(server, user=user)

def exec_command(conn, command):
    print("Running Command: ", command)
    res = conn.run(command, warn=True, hide='both', pty=True)
    if res.stderr:
        raise Exception(res.stderr)

    for line in res.stdout.split("\n"):
        print(line)

def update_repo(conn, hash_id):
    command = "cd ~/sampleapp && git pull origin master"
    exec_command(conn, command)

    command = "cd ~/sampleapp && git reset --hard {0}".format(hash_id)
    exec_command(conn, command)

def run_docker(conn):
    command = "cd ~/sampleapp && cat ~/docker_pass.txt | docker login -u yspanchal --password-stdin"
    exec_command(conn, command)

    command = "cd ~/sampleapp && sed -i -E 's|yspanchal/sampleapp_app:.*|yspanchal/sampleapp_app:'`git log -1 --pretty=%H`'|g' docker-compose.yml"
    exec_command(conn, command)

    command = "cd ~/sampleapp && docker-compose up -d"
    exec_command(conn, command)

    command = "cd ~/sampleapp && docker-compose ps"
    exec_command(conn, command)

def clean_env(conn):
    command = "cd ~/sampleapp && docker-compose down"
    exec_command(conn, command)

    command = "cd ~/sampleapp && docker image prune -a -f"
    exec_command(conn, command)

@task(optional=['hash'])
def deploy(ctx, hash):
    for server in servers:
        print("Deploying on server: {0}".format(server))
        conn = Connection(server, user=user)
        clean_env(conn)
        update_repo(conn, hash)
        run_docker(conn)
        print("Deployment Completed on server: {0}".format(server))
    print("Over All Deployment Completed.")

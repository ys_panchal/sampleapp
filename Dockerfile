FROM python:3.7
ENV PYTHONUNBUFFERED 1

RUN mkdir /Code

WORKDIR /Code

COPY requirements.txt /Code/

RUN pip3 install -r requirements.txt

COPY . /Code/

ENV AUTHOR="Yogesh Panchal <yspanchal@gmail.com>"

EXPOSE 8000

CMD /code/run.sh
